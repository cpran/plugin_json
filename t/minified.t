include ../../plugin_tap/procedures/more.proc
include ../../plugin_json/procedures/write.proc

json.minify = 1

@plan: 13

@hash()
hash = hash.id

@keyref: "a", 1

@is$: do$("Get value...", 1, "a"), string$(1) + json.id_carat$,
  ... "can store a reference"

@is: do("Get value...", 1, "a"), 1,
  ... "numerification of reference is reference"

@keyref: "a", undefined

@is: do("Get value...", 1, "a"), undefined,
  ... "undefined reference stored as undefined"

removeObject: hash

# { "a": 1 }

@hash()
sub = hash.id
@keyval: "a", 1

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "{""a"":1}",
  ... "JSON object"

# { "a": null }

@keyval: "a", undefined

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "{""a"":null}",
  ... "JSON object with null value"

# {
#   "a": 1,
#   "b": 2
# }

@keyval: "a", 1
@keyval: "b", 2

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "{""a"":1,""b"":2}",
  ... "JSON object with multiple elements"

# {
#   "A": {
#     "a": 1,
#     "b": 2
#   }
# }

@hash()
main = hash.id
@keyref:  "A", sub

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "{""A"":{""a"":1,""b"":2}}",
  ... "JSON object with sub elements"

# {
#   "A": {
#     "a": {
#       "x": 5
#     }
#   }
# }

selectObject: sub
Remove column: "b"

@hash()
subsub = hash.id
@keyval: "x", 5

selectObject: sub
@keyref: "a", subsub

selectObject: main

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "{""A"":{""a"":{""x"":5}}}",
  ... "JSON object with 2-level elements"

# {
#   "A": {
#     "a": {
#       "x": 5
#     }
#   },
#   "B": 4
# }

selectObject: main
@keyval: "B", 4

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "{""A"":{""a"":{""x"":5}},""B"":4}",
  ... "JSON object with 2-level elements"

removeObject: main, sub, subsub

# [
#   { "a": 1 },
#   { "a": 2 },
#   { "a": 3 },
#   { "a": 4 },
#   { "a": 5 }
# ]

@hash()
main = hash.id

@keyval: "a", 1
for i from 2 to 5
  Append row
  @keyval: "a", i
endfor

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "[{""a"":1},{""a"":2},{""a"":3},{""a"":4},{""a"":5}]",
  ... "JSON object list as rows in table-hash"

removeObject: main

# [ 1, 2, 3, 4, 5 ]

@array()
list = array.id
for i to 5
  @push: i
endfor

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "[1,2,3,4,5]",
  ... "JSON list from strings-array"

removeObject: list

# [
#   { "a": 1 },
#   { "a": 2 },
#   { "a": 3 },
#   { "a": 4 },
#   { "a": 5 }
# ]

@array()
list = array.id

for i to 5
  @hash()
  dict[i] = hash.id
  @keyval: "a", i

  selectObject: list
  @push$: string$(dict[i]) + json.id_carat$
endfor

clearinfo
@write_json()

json$ = info$() - newline$
@is$: json$, "[{""a"":1},{""a"":2},{""a"":3},{""a"":4},{""a"":5}]",
  ... "JSON object list as table-hash references in strings-array"

removeObject: list
for i to 5
  removeObject: dict[i]
endfor

@ok_selection()

@done_testing()
