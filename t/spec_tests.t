include ../../plugin_tap/procedures/more.proc
include ../../plugin_json/procedures/read.proc

include ../../plugin_utils/procedures/try.proc
try.remove_on_fail = 1

@plan: 37

list = Create Strings as file list: "tests", "tests/*json"
tests = Get number of strings

for i to tests
  selectObject: list
  filename$ = Get string: i

  call try
    ... include ../../plugin_json/procedures/read.proc \n
    ... json.max_depth = 19                            \n
    ... json$ = readFile$("tests/" + "'filename$'")    \n
    ... @read_json: json$

  if index_regex(filename$, "^fail2[578]\.json$")
    @skip: 1, "Praat whitespace handling is special"
  endif

  if left$(filename$) == "p"
    @is_false: try.catch, filename$
  else
    @is_true:  try.catch, filename$
  endif

  if !try.catch
    @remove_deeply()
  endif
endfor

removeObject: list

@ok_selection()

@done_testing()
