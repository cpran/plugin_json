include ../../plugin_tap/procedures/more.proc
include ../../plugin_json/procedures/read.proc
include ../../plugin_json/procedures/write.proc

json.minify = 1

@plan: 56

read_json_value.done = 0
@read_json_value: "1"
@is: read_json_value.done, 1,
  ... "numeric value is done"
@is$: read_json_value.value$, "1",
  ... "value string is correct number"
@is$: read_json_value.typeof$,  "number",
  ... "value has number type"

read_json_value.done = 0
@read_json_value: """hello"""
@is: read_json_value.done, 1,
  ... "string value is done"
@is$: read_json_value.value$, """hello""",
  ... "value string is correct string"
@is$: read_json_value.typeof$,  "string",
  ... "value has string type"

read_json_value.done = 0
@read_json_value: "null"
@is: read_json_value.done, 1,
  ... "null value is done"
@is$: read_json_value.value$, "null",
  ... "value string is correct null"
@is$: read_json_value.typeof$,  "null",
  ... "value has null type"

read_json_value.done = 0
@read_json_value: "false"
@is: read_json_value.done, 1,
  ... "null value is done"
@is$: read_json_value.value$, "false",
  ... "value string is correct false"
@is$: read_json_value.typeof$,  "boolean",
  ... "value has boolean type"

read_json_value.done = 0
@read_json_value: "true"
@is: read_json_value.done, 1,
  ... "null value is done"
@is$: read_json_value.value$, "true",
  ... "value string is correct true"
@is$: read_json_value.typeof$,  "boolean",
  ... "value has boolean type"

read_json_value.done = 0
@read_json_value: "{}"
@is: read_json_value.done, 1,
  ... "object value is done"
@like: read_json_value.value$, "[0-9]+#",
  ... "value string is correct object"
@is$: read_json_value.typeof$,  "object",
  ... "value has object type"
@is: numberOfSelected("Table"), 1,
  ... "created a Table"
Remove

read_json_value.done = 0
@read_json_value: "[]"
@is: read_json_value.done, 1,
  ... "object value is done"
@like: read_json_value.value$, "[0-9]+#",
  ... "value string is correct array"
@is$: read_json_value.typeof$,  "array",
  ... "value has array type"
@is: numberOfSelected("Strings"), 1,
  ... "created a Strings"
Remove


call multiline {}
@mytest()
@is$: info$() - newline$, multiline.str$, "Empty object"

call multiline {"foo":null}
@mytest()
@is$: info$() - newline$, multiline.str$, "Null"

call multiline {"foo":true}
@mytest()
call multiline {"foo":1}
@is$: info$() - newline$, multiline.str$, "True"

call multiline {"foo":false}
@mytest()
call multiline {"foo":0}
@is$: info$() - newline$, multiline.str$, "False"

call multiline {"foo":1}
@mytest()
@is$: info$() - newline$, multiline.str$, "Int"

call multiline {"foo":-1}
@mytest()
@is$: info$() - newline$, multiline.str$, "Negative int"

call multiline {"foo":+1}
@mytest()
call multiline {"foo":1}
@is$: info$() - newline$, multiline.str$, "Positive int"

call multiline {"foo":0.1}
@mytest()
@is$: info$() - newline$, multiline.str$, "Frac"

call multiline {"foo":-0.24}
@mytest()
@is$: info$() - newline$, multiline.str$, "Negative frac"

call multiline {"foo":+0.431}
@mytest()
call multiline {"foo":0.431}
@is$: info$() - newline$, multiline.str$, "Positive frac"

call multiline {"foo":1e16}
@mytest()
call multiline {"foo":1e+16}
@is$: info$() - newline$, multiline.str$, "Int exp"

call multiline {"foo":1.2e-16}
@mytest()
@is$: info$() - newline$, multiline.str$, "Int negative exp"

call multiline {"foo":1.2e+16}
@mytest()
@is$: info$() - newline$, multiline.str$, "Int positive exp"

call multiline {"foo":7.26e16}
@mytest()
call multiline {"foo":7.26e+16}
@is$: info$() - newline$, multiline.str$, "Frac exp"

call multiline {"foo":7.26e-16}
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Frac negative exp"

call multiline {"foo":7.26e+16}
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Frac positive  exp"

call multiline {"foo":"hello"}
@mytest()
@is$: info$() - newline$, multiline.str$, "String"

call multiline {"foo":""}
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Empty string"

call multiline {"foo":"\""}
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "String with escaped characters"

call multiline {"f\"oo":"bar"}
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Key with escaped characters"

call multiline {"foo":{"bar":null}}
@mytest()
@is$: info$() - newline$, multiline.str$, "Nested objects"

call multiline {"foo":1,"bar":null}
@mytest()
@is$: info$() - newline$, multiline.str$, "Object with multiple elements"

call multiline {"foo":[{"bar":8},{"bar":2}]}
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Object with list of objects"

call multiline [null,1,{"a":0},"foo",{"foo":[{"bar":8},{"bar":2}]}]
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "JSON smorgasbord"

call multiline []
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Empty array"

call multiline [1]
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Array with integer"

call multiline [1,3,4]
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Array with multiple integers"

call multiline ["bar"]
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Array with string"

call multiline ["foo","bar","baz"]
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Array with multiple strings"

call multiline [[""]]
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Array with empty string"

call multiline [["bar"]]
@mytest()
@is$: info$() - newline$, multiline.str$,
  ... "Array with array with string"

call multiline:
  ...[        \n
  ...  "foo", \n
  ...  "bar"  \n
  ...]
@mytest()
call multiline ["foo","bar"]
@is$: info$() - newline$, multiline.str$,
  ... "Multiline string"

@ok_selection()

@done_testing()

procedure multiline: .str$
  .str$ = replace_regex$(.str$, "\s*\\n", newline$, 0)
  .str$ = replace_regex$(.str$, "  " , json.indent$, 0)
endproc

procedure mytest()
  @read_json: multiline.str$
  clearinfo
  @write_json()
  @remove_deeply()
endproc
