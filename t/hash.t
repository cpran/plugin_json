include ../../plugin_tap/procedures/more.proc
include ../../plugin_json/procedures/write.proc

@plan: 13

@hash()
@is: numberOfSelected("Table"), 1,
  ... "hash Table created"
hash = hash.id

@keyval: "a", 4
@keyval: "b", 2
@keyval: "c", 3
@keyval: "a", 1

@is: do("Get number of columns"), 3,
  ... "table has correct number of columns"

@is: do("Get number of rows"), 1,
  ... "table has correct number of rows"

list$ = List: "no"
columns$ = extractLine$(list$, "")
@is$: columns$, "a" + tab$ + "b" + tab$ + "c",
  ... "columns have correct labels"

if    do("Get value...", 1, "a") == 1 and
  ... do("Get value...", 1, "b") == 2 and
  ... do("Get value...", 1, "c") == 3

  @pass: "keys have proper values"
else
  @fail: "keys have proper values"
endif

@keyval$: "a", "string"
@is$: do$("Get value...", 1, "a"), "string",
  ... "stored a string in table"

@is: do("Get value...", 1, "a"), undefined,
  ... "numerification of string is undefined"

@keyval$: "a", ""
@is$: do$("Get value...", 1, "a"), "",
  ... "can store empty string"

@keyval: "a", undefined
@is: do("Get value...", 1, "a"), undefined,
  ... "can store undefined numeric"

@keyval: "a", 1

Append row

@is: do("Get value...", 2, "a"), undefined,
  ... "new row has undefined value"

@keyval: "a", 5

@is: do("Get value...", 2, "a"), 5,
  ... "keyval stores in last row"

@is: do("Get value...", 1, "a"), 1,
  ... "previous row is unaffected"

removeObject: hash

@ok_selection()

@done_testing()
