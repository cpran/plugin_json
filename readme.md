json
====

Description
-----------

[JSON][] is a standard for data serialisation that has seen
widespread use in the exchange of arbitrarily complex data
structures across all sorts of software tools. It allows you to
write these data in a format that is not application-dependant,
and share them with whatever you choose.

The JSON plugin provides Praat with a much needed interface to
produce JSON output from internal data structures. It uses Table
objects as dictionaries and Strings objects as lists, and provides
procedures to easily print the contents of these as JSON-compliant
output.

The methods included allow for objects to reference other
objects, making it possible to represent arbitrarily
complex data structures.

Additionally, this plugin includes procedures to interact with
Table objects as if they were a dictionary, in a way that
complements the methods offered by the `array.proc` file in the
["strutils"][strutils] plugin.

Requirements
------------

* `strutils`

[json]: http://json.org
[strutils]: http://cpran.net/plugins/strutils
